#Installation


**$WiresharkDir** references to the following directories on the following platforms:

**Windows**

```%APPDATA%\Wireshark```

**\*nix**

```~/.wireshark```


Download all 6 GeoIP databases from [http://dev.maxmind.com/geoip/legacy/geolite/]()

Extract all dat files to ```~/GeoIP```

Edit ```$WiresharkDir/geoip_db_paths```, replace the current path with the **full path** of ```~/GeoIP```, surrounded by double quotes.  


Clone or download this repo and unpack it to $WiresharkDir.

Launch Wireshark.